<?php

/* set time zone */
date_default_timezone_set("Asia/Kolkata");

require_once '../helpers/debug_helper.php';

$project = $_SERVER['QUERY_STRING'];

if( empty($project))
{
	echo 'Project name is empty..Set project name using <b>"?project_name"</b>';
	exit;
}

$project_base_path = '../../' . $project;

if( ! is_dir($project_base_path))
{
	echo 'Project base path does not exist <b>' . $project_base_path . '".</b>Check the project name OR check folder exists or not.';
	exit;
}

$input_folder = $project_base_path . '/migration_generator/';

if( ! is_dir($input_folder))
{
	echo 'Unable to access input folder <b>' . $input_folder . '".</b>Check the project name OR check folder exists or not.';
	exit;
}

$input_file = $project . '_alterations.xlsx';

if( ! file_exists($input_folder . $input_file))
{
	echo 'Input excel file <b>"' . $input_folder . $input_file . '</b> does not exist. Check the file or file name.';
	exit;
}

/* load PHPExcel lib */
require_once '../libraries/PHPExcel.php';

/* generate version time */
$now = getdate();
$version_time = mktime($now['hours'], $now['minutes'], $now['seconds'], $now['mon'], $now['mday'], $now['year']);


/* set output folder */
$output_folder = $project_base_path . '/application/migrations/';

/* generate output folder if not generated */
if( ! is_dir($output_folder))
{
	if(mkdir($output_folder, 0755, true))
	{
		echo "$output_folder created successfully. <br/><br/>";
	}
}

/*  set backup folder */
$backup_folder = $project_base_path . '/migration_backup/' . date('Y-m-d-H-i-s',$version_time) . '_alterations/';

/* generate backup folder if not generated */
if( ! is_dir($backup_folder))
{
	if(mkdir($backup_folder, 0755, true))
	{
		echo "$backup_folder created successfully. <br/><br/>";
	}
}

/* load file helper */
require_once '../helpers/file_helper.php';

/* identify file type and read file */
$file_type	= PHPExcel_IOFactory::identify($input_folder . $input_file);
$objReader	= PHPExcel_IOFactory::createReader($file_type);
$objPHPExcel= $objReader->load($input_folder . $input_file);

// Get total sheet
$i = $fail_cnt = $success_cnt = $bk_fail_cnt = $bk_success_cnt = $already_generated_cnt = 0;
foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
{
	$worksheet_title = $worksheet->getTitle();
	$file_name = ucfirst(strtolower($worksheet_title));
	$sheetIndex = $objPHPExcel->getIndex($worksheet);
	// Set sheet index
	$sheet_data	= $objPHPExcel->setActiveSheetIndex($sheetIndex)->toArray(null,true,true,true);

	$is_generated = $sheet_data[2]['A'];
	$table_name = $sheet_data[2]['B'];
	if( ! $is_generated)
	{
		$add_fields_up = $modify_fields_up = $up_field = $down_field = $drop_fields = $modify_fields_down = $delete_fields_up = $delete_fields_down = '';
		$add_foreign_keys = $modify_foreign_keys = $drop_foreign_keys = $add_constraient_nos = $modify_constraient_nos = $drop_constraint_nos = [];

		// Add fields to array
		foreach($sheet_data as $index => $line)
		{
			if($index != 1)
			{
				$alteration_type	= $line['C'];
				$old_field			= $line['D'];
				$new_field			= $line['E'];
				$old_after_field	= $line['F'];
				$new_after_field	= $line['G'];
				$old_data_type		= $line['H'];
				$new_data_type		= $line['I'];
				$old_size			= $line['J'];
				$new_size			= $line['K'];
				$old_auto_inc		= $line['L'];
				$new_auto_inc		= $line['M'];
				$old_constraint_no	= $line['N'];
				$new_constraint_no	= $line['O'];
				$old_foreign_key	= $line['P'];
				$new_foreign_key	= $line['Q'];
				$old_unsigned		= $line['R'];
				$new_unsigned		= $line['S'];
				$old_is_null		= $line['T'];
				$new_is_null		= $line['U'];
				$old_default		= $line['V'];
				$new_default		= $line['W'];
				$old_comment		= $line['X'];
				$new_comment		= $line['Y'];

				if($alteration_type == 'add' || $alteration_type == 'modify' || $alteration_type == 'delete')
				{
					$add_foreign_key = $add_constraient_no = [];

					if($alteration_type == 'modify')
					{
						/* for up function */
						$up_field = "\t\t\t" . "'" . $old_field . "' => array(" . PHP_EOL;
						! empty($new_field) ? $up_field .= "\t\t\t\t" . "'name' => '" . $new_field . "'," . PHP_EOL : "";

						/* for down function */
						$down_field = "\t\t\t" . "'" . $new_field . "' => array(" . PHP_EOL;
						! empty($old_field) ? $down_field .= "\t\t\t\t" . "'name' => '" . $old_field . "'," . PHP_EOL : "";
						! empty($old_data_type) ? $down_field .= "\t\t\t\t" . "'type' => '" . $old_data_type . "'," . PHP_EOL : "";
						! empty($old_size) ? $down_field .= "\t\t\t\t" . "'constraint' => '" . $old_size . "'," . PHP_EOL : "";
						! empty($old_auto_inc) ? $down_field .= "\t\t\t\t" . "'auto_increment' => TRUE," . PHP_EOL : "";
						! empty($old_unsigned) ? $down_field .= "\t\t\t\t" . "'unsigned' => TRUE," . PHP_EOL : "";
						! empty($old_is_null) ? $down_field .= "\t\t\t\t" . "'null' => TRUE," . PHP_EOL : "";
						$old_default != '' ? $down_field .= "\t\t\t\t" . "'default' => " . $old_default . "," . PHP_EOL : "";
						! empty($old_comment) ? $down_field .= "\t\t\t\t" . "'comment' => '" . $old_comment . "',". PHP_EOL : "";
						$down_field .= "\t\t\t".'),' . PHP_EOL;
					}

					if($alteration_type == 'add')
					{
						/* for up function */
						$up_field = "\t\t\t" . "'" . $new_field . "' => array(" . PHP_EOL;
						! empty($new_after_field) ? $up_field .= "\t\t\t\t"."'after' => '" . $new_after_field. "'," . PHP_EOL : "";

						/* for down function */
						$down_field = "\t\t" . '$this->dbforge->drop_column("' . $table_name .'", "' . $new_field. '");' . PHP_EOL;
					}
					
					if($alteration_type == 'delete')
					{
						/* for up function */
						$up_field = "\t\t" . '$this->dbforge->drop_column("' . $table_name .'", "' . $old_field. '");' . PHP_EOL;
						
						/* for down function */
						$down_field = "\t\t\t" . "'" . $old_field . "' => array(" . PHP_EOL;
						! empty($old_after_field) ? $down_field .= "\t\t\t\t"."'after' => '" . $old_after_field. "'," . PHP_EOL : "";
						! empty($old_data_type) ? $down_field .= "\t\t\t\t" . "'type' => '" . $old_data_type . "'," . PHP_EOL : "";
						! empty($old_size) ? $down_field .= "\t\t\t\t" . "'constraint' => '" . $old_size . "'," . PHP_EOL : "";
						! empty($old_auto_inc) ? $down_field .= "\t\t\t\t" . "'auto_increment' => TRUE," . PHP_EOL : "";
						! empty($old_unsigned) ? $down_field .= "\t\t\t\t" . "'unsigned' => TRUE," . PHP_EOL : "";
						! empty($old_is_null) ? $down_field .= "\t\t\t\t" . "'null' => TRUE," . PHP_EOL : "";
						$old_default != '' ? $down_field .= "\t\t\t\t" . "'default' => " . $old_default . "," . PHP_EOL : "";
						! empty($old_comment) ? $down_field .= "\t\t\t\t" . "'comment' => '" . $old_comment . "',". PHP_EOL : "";						
						$down_field .= "\t\t\t".'),' . PHP_EOL;
					}

					/* for up function. Common for both add and modify */
					if($alteration_type == 'add' || $alteration_type == 'modify')
					{
						! empty($new_data_type) ? $up_field .= "\t\t\t\t" . "'type' => '" . $new_data_type . "'," . PHP_EOL : "";
						! empty($new_size) ? $up_field .= "\t\t\t\t" . "'constraint' => '" . $new_size . "'," . PHP_EOL : "";
						! empty($new_auto_inc) ? $up_field .= "\t\t\t\t" . "'auto_increment' => TRUE," . PHP_EOL : "";
						! empty($new_unsigned) ? $up_field .= "\t\t\t\t" . "'unsigned' => TRUE," . PHP_EOL : "";
						! empty($new_is_null) ? $up_field .= "\t\t\t\t" . "'null' => TRUE," . PHP_EOL : "";
						$new_default != '' ? $up_field .= "\t\t\t\t" . "'default' => " . $new_default . "," . PHP_EOL : "";
						! empty($new_comment) ? $up_field .= "\t\t\t\t" . "'comment' => '" . $new_comment . "',". PHP_EOL : "";
						$up_field .= "\t\t\t".'),' . PHP_EOL;
					}

					if( ! empty($new_foreign_key))
					{
						$add_foreign_key[$new_field] = $new_foreign_key;
					}
					
					if( ! empty($old_foreign_key))  
					{
						$drop_foreign_keys[$old_field] = $old_foreign_key;
					}

					if( ! empty($new_constraint_no))
					{
						$add_constraient_no[$new_field] = $new_constraint_no;
					}

					if( ! empty($old_constraint_no))
					{
						$drop_constraint_nos[$old_field] = $old_constraint_no;
					}

					if($alteration_type == 'modify')
					{
						$modify_fields_up .= $up_field;
						$modify_fields_down .= $down_field;
						$modify_foreign_keys = array_merge($modify_foreign_keys, $add_foreign_key);
						$modify_constraient_nos = array_merge($modify_constraient_nos, $add_constraient_no);
					}

					if($alteration_type == 'add')
					{
						$add_fields_up .= $up_field;
						$drop_fields .= $down_field;
						$add_foreign_keys = array_merge($add_foreign_keys, $add_foreign_key);
						$add_constraient_nos = array_merge($add_constraient_nos, $add_constraient_no);
					}

					if($alteration_type == 'delete')
					{
						$delete_fields_up .= $up_field;
						$delete_fields_down .= $down_field;
						//$add_foreign_keys = array_merge($add_foreign_keys, $add_foreign_key);
						//$add_constraient_nos = array_merge($add_constraient_nos, $add_constraient_no);
					}
				}
			}
		}
		
		$file_content = '<?php defined(\'BASEPATH\') OR exit(\'No direct script access allowed\');' . PHP_EOL . PHP_EOL;
		$file_content .= 'class Migration_' . $file_name . ' extends CI_Migration' . PHP_EOL;
		$file_content .= '{'. PHP_EOL;
		/* Add up function content */
		$file_content .= "\t" . 'public function up()' . PHP_EOL;
		$file_content .= "\t" . '{' . PHP_EOL;

		if( ! empty($modify_fields_up))
		{
			$file_content .= "\t\t" . '$fields = array(' . PHP_EOL;
			$file_content .= $modify_fields_up;
			$file_content .= "\t\t" .');' . PHP_EOL . PHP_EOL;
			$file_content .= "\t\t" .'$this->dbforge->modify_column("' . $table_name . '", $fields);' . PHP_EOL ;
			
			/* drop old foreign key constraint */
			foreach($drop_constraint_nos as $constraient_no)
			{
				$file_content .= "\t\t" . '$this->db->query("ALTER TABLE {PRE}' . $table_name . ' DROP FOREIGN KEY {PRE}' . $table_name . '_ibfk_' . $constraient_no . '");'. PHP_EOL;
				$file_content .= "\t\t" . '$this->db->query("ALTER TABLE {PRE}' . $table_name . ' DROP INDEX {PRE}' . $table_name . '_ibfk_' . $constraient_no . '");'. PHP_EOL;
			}
			
			/* Set foreign keys */
			foreach($modify_foreign_keys as $key => $value)
			{
				$file_content .= "\t\t" . '$this->dbforge->add_column("' . $table_name . '", "CONSTRAINT {PRE}' . $table_name . '_ibfk_' . $modify_constraient_nos[$key] . ' FOREIGN KEY ('.$key.') REFERENCES {PRE}'.$value.'");'. PHP_EOL;
			}
			$file_content .= PHP_EOL;
		}

		if( ! empty($add_fields_up))
		{
			$file_content .= "\t\t" . '$fields = array(' . PHP_EOL;
			$file_content .= $add_fields_up;
			$file_content .= "\t\t" .');' . PHP_EOL . PHP_EOL;
			$file_content .= "\t\t" .'$this->dbforge->add_column("' . $table_name . '", $fields);' . PHP_EOL ;
			/* Set foreign keys */
			foreach($add_foreign_keys as $key => $value)
			{
				$file_content .= "\t\t" . '$this->dbforge->add_column("' . $table_name . '", "CONSTRAINT {PRE}' . $table_name . '_ibfk_' . $add_constraient_nos[$key] . ' FOREIGN KEY ('.$key.') REFERENCES {PRE}'.$value.'");'. PHP_EOL;
			}
			$file_content .= PHP_EOL;
		}
		
		if( ! empty($delete_fields_up))
		{
			/* Drop foreign keys if any */
			foreach($drop_constraint_nos as $constraient_no)
			{
				$file_content .= "\t\t" . '$this->db->query("ALTER TABLE {PRE}' . $table_name . ' DROP FOREIGN KEY {PRE}' . $table_name . '_ibfk_' . $constraient_no . '");'. PHP_EOL;
				$file_content .= "\t\t" . '$this->db->query("ALTER TABLE {PRE}' . $table_name . ' DROP INDEX {PRE}' . $table_name . '_ibfk_' . $constraient_no . '");'. PHP_EOL;
			}
			$file_content .= $delete_fields_up;
			$file_content .= PHP_EOL;
		}

		$file_content .= "\t" . '}' . PHP_EOL . PHP_EOL;

		/* add down function content */
		$file_content .= "\t" . 'public function down()' . PHP_EOL;
		$file_content .= "\t" . '{' . PHP_EOL;

		if( ! empty($drop_fields))
		{
			foreach($add_constraient_nos as $constraient_no)
			{
				$file_content .= "\t\t" . '$this->db->query("ALTER TABLE {PRE}' . $table_name . ' DROP FOREIGN KEY {PRE}' . $table_name . '_ibfk_' . $constraient_no . '");'. PHP_EOL;
				$file_content .= "\t\t" . '$this->db->query("ALTER TABLE {PRE}' . $table_name . ' DROP INDEX {PRE}' . $table_name . '_ibfk_' . $constraient_no . '");'. PHP_EOL;
			}
			$file_content .= $drop_fields;
			$file_content .= PHP_EOL;
		}

		if( ! empty($modify_fields_down))
		{
			foreach($modify_constraient_nos as $constraient_no)
			{
				$file_content .= "\t\t" . '$this->db->query("ALTER TABLE {PRE}' . $table_name . ' DROP FOREIGN KEY {PRE}' . $table_name . '_ibfk_' . $constraient_no . '");'. PHP_EOL;
				$file_content .= "\t\t" . '$this->db->query("ALTER TABLE {PRE}' . $table_name . ' DROP INDEX {PRE}' . $table_name . '_ibfk_' . $constraient_no . '");'. PHP_EOL;
			}
			
			$file_content .= "\t\t" . '$fields = array(' . PHP_EOL;
			$file_content .= $modify_fields_down;
			$file_content .= "\t\t" .');' . PHP_EOL . PHP_EOL;
			$file_content .= "\t\t" .'$this->dbforge->modify_column("' . $table_name . '", $fields);' . PHP_EOL;
			foreach($drop_foreign_keys as $key => $value)
			{
				$file_content .= "\t\t" . '$this->dbforge->add_column("' . $table_name . '", "CONSTRAINT {PRE}' . $table_name . '_ibfk_' . $drop_constraint_nos[$key] . ' FOREIGN KEY (' . $key . ') REFERENCES {PRE}' . $value . '");'. PHP_EOL;
			}
		}
		
		if( ! empty($delete_fields_down))
		{
			$file_content .= "\t\t" . '$fields = array(' . PHP_EOL;
			$file_content .= $delete_fields_down;
			$file_content .= "\t\t" .');' . PHP_EOL . PHP_EOL;
			$file_content .= "\t\t" .'$this->dbforge->add_column("' . $table_name . '", $fields);' . PHP_EOL ;
			/* Set foreign keys */
			foreach($drop_foreign_keys as $key => $value)
			{
				$file_content .= "\t\t" . '$this->dbforge->add_column("' . $table_name . '", "CONSTRAINT {PRE}' . $table_name . '_ibfk_' . $drop_constraint_nos[$key] . ' FOREIGN KEY ('.$key.') REFERENCES {PRE}'.$value.'");'. PHP_EOL;
			}
			$file_content .= PHP_EOL;
		}

		$file_content .= "\t" . '}' . PHP_EOL;

		$file_content .= '}' . PHP_EOL;
		$file_content .= '?>';

		// xdebug($file_content);

		/* Write File to output folder */
		if ( ! write_file($output_folder . date('YmdHis', $version_time + $i) . '_' . strtolower($file_name) . '.php', $file_content))
		{
			$fail_cnt++;
			$objPHPExcel->getActiveSheet()->setCellValue('A2', 'FALSE');
		}
		else
		{
			$success_cnt++;
			$objPHPExcel->getActiveSheet()->setCellValue('A2', 'TRUE');
		}

		/* Write File to backup folder*/
		if ( ! write_file($backup_folder . date('YmdHis', $version_time + $i) . '_' . strtolower($file_name) .'.php', $file_content))
		{
			$bk_fail_cnt ++;
		}
		else
		{
			$bk_success_cnt ++;
		}
		$i++;
	}
	else
	{
		$already_generated_cnt++;
		$already_generated_files[] = $worksheet_title;
	}
}

/* update flag is_generated */
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $file_type);
$objWriter->save($input_folder . $input_file);

/* output folder results */
if( ! empty($fail_cnt))
{
	echo 'Failed to generate: ' . $fail_cnt . ' file(s)';
	echo '<br/>';
}
if( ! empty($success_cnt))
{
	echo $success_cnt . ' file(s) generated in folder' . $output_folder;
	echo '<br/>';
}

/* backup folder results */
if( ! empty($bk_fail_cnt))
{
	echo 'Failed to generate: ' . $bk_fail_cnt . ' file(s)';
	echo '<br/>';
}
if( ! empty($bk_success_cnt))
{
	echo $bk_success_cnt . ' file(s) generated in folder' . $backup_folder;
	echo '<br/>';
}

/* already generated file data */
if( ! empty($already_generated_cnt))
{
	echo $already_generated_cnt . ' file(s) already generated';
	echo '<br/>';
}

if( ! empty($already_generated_files))
{
	echo 'List of file(s) are as follows: ';
	echo '<br/>';
	foreach($already_generated_files as $i => $file)
	{
		echo $i+1 . '. ' . $file;
		echo '<br/>';
	}
}

if(empty($success_cnt) && empty($bk_success_cnt))
{
	if(rmdir($backup_folder))
	{
		echo "<br/>No new files generated. <br/> Deleted $backup_folder since folder is empty.";
	}
}
?>