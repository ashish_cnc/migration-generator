<?php

/* set time zone */
date_default_timezone_set("Asia/Kolkata");

require_once '../helpers/debug_helper.php';

$project = $_SERVER['QUERY_STRING'];

if (empty($project))
{
	exit('Project name is empty..Set project name using <b>"?project_name"</b>');
}

$project_base_path = '../../'.$project;

if ( ! is_dir($project_base_path))
{
	exit('Project base path does not exist <b>'.$project_base_path.'".</b>Check the project name OR check folder exists or not.');
}

$input_folder = $project_base_path.'/migration_generator/';

if ( ! is_dir($input_folder))
{
	exit('Unable to access input folder <b>'.$input_folder.'".</b>Check the project name OR check folder exists or not.');
}

$input_file = $project.'.xlsx';

if ( ! file_exists($input_folder.$input_file))
{
	exit('Input excel file <b>"'.$input_folder.$input_file.'</b> does not exist. Check the file or file name.');
}

/* load PHPExcel lib */
require_once '../libraries/PHPExcel.php';

/* generate version time */
$now          = getdate();
$version_time = mktime($now['hours'], $now['minutes'], $now['seconds'], $now['mon'], $now['mday'], $now['year']);

/* set output folder */
$output_folder = $project_base_path.'/application/migrations/';

/* generate output folder if not generated */

if ( ! is_dir($output_folder))
{
	if (mkdir($output_folder, 0755, true))
	{
		echo "$output_folder created successfully. <br/><br/>";
	}
}

/*  set backup folder */
$backup_folder = $project_base_path.'/migration_backup/'.date('Y-m-d-H-i-s', $version_time).'/';

/* generate backup folder if not generated */

if ( ! is_dir($backup_folder))
{
	if (mkdir($backup_folder, 0755, true))
	{
		echo "$backup_folder created successfully. <br/><br/>";
	}
}

/* identify file type and read file */
$file_type   = PHPExcel_IOFactory::identify($input_folder.$input_file);
$objReader   = PHPExcel_IOFactory::createReader($file_type);
$objPHPExcel = $objReader->load($input_folder.$input_file);

/* load file helper */
require_once '../helpers/file_helper.php';

$i = $fail_cnt = $success_cnt = $bk_fail_cnt = $bk_success_cnt = $already_generated_cnt = 0;

/* Get total sheet */

foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
{
	$file_name  = $worksheet->getTitle();
	$sheetIndex = $objPHPExcel->getIndex($worksheet);

	/* Set sheet index */
	$sheet_data   = $objPHPExcel->setActiveSheetIndex($sheetIndex)->toArray(null, true, true, true);
	$is_generated = $sheet_data[2]['A'];

	if ( ! $is_generated)
	{
		$file_content = '<?php defined(\'BASEPATH\') OR exit(\'No direct script access allowed\');'.PHP_EOL.PHP_EOL;

		$file_content .= 'class Migration_Add_'.$file_name.' extends CI_Migration'.PHP_EOL;
		$file_content .= '{'.PHP_EOL;

		/* Add "Up" function */
		$file_content .= "\t".'public function up()'.PHP_EOL;
		$file_content .= "\t".'{'.PHP_EOL;

		/* Start adding fields */
		$file_content .= "\t\t".'$this->dbforge->add_field(array('.PHP_EOL;

		$primary_keys = $foreign_keys = $constraint_nos = [];

		/* Add fields to array */
		foreach ($sheet_data as $index => $line)
		{
			if ($index != 1)
			{
				$field         = $line['B'];
				$data_type     = $line['C'];
				$size          = $line['D'];
				$auto_inc      = $line['E'];
				$is_primary    = $line['F'];
				$constraint_no = $line['G'];
				$foreign_key   = $line['H'];
				$unsigned      = $line['I'];
				$is_null       = $line['J'];
				$default       = $line['K'];
				$comment       = $line['L'];

				$file_content .= "\t\t\t"."'".$field."' => array(".PHP_EOL;
				! empty($data_type) ? $file_content .= "\t\t\t\t"."'type' => '".$data_type."',".PHP_EOL : "";
				! empty($size) ? $file_content .= "\t\t\t\t"."'constraint' => '".$size."',".PHP_EOL : "";
				! empty($auto_inc) ? $file_content .= "\t\t\t\t"."'auto_increment' => TRUE,".PHP_EOL : "";
				! empty($unsigned) ? $file_content .= "\t\t\t\t"."'unsigned' => TRUE,".PHP_EOL : "";
				! empty($is_null) ? $file_content .= "\t\t\t\t"."'null' => TRUE,".PHP_EOL : "";
				$default != '' ? $file_content .= "\t\t\t\t"."'default' => ".$default.",".PHP_EOL : "";
				! empty($comment) ? $file_content .= "\t\t\t\t"."'comment' => '".$comment."',".PHP_EOL : "";
				$file_content .= "\t\t\t".'),'.PHP_EOL;

				if ( ! empty($is_primary))
				{
					$primary_keys[] = $field;
				}

				if ( ! empty($foreign_key))
				{
					$foreign_keys[$field] = $foreign_key;
				}

				if ( ! empty($constraint_no))
				{
					$constraint_nos[$field] = $constraint_no;
				}
			}
		}

		$file_content .= "\t\t".'));'.PHP_EOL.PHP_EOL;

		/* Set Primary keys or composite keys */

		foreach ($primary_keys as $primary_key)
		{
			$file_content .= "\t\t".'$this->dbforge->add_key("'.$primary_key.'", TRUE);'.PHP_EOL;
		}

		/* Set foreign keys */

		foreach ($foreign_keys as $key => $value)
		{
			$file_content .= "\t\t".'$this->dbforge->add_field("CONSTRAINT {PRE}'.$file_name.'_ibfk_'.$constraint_nos[$key].' FOREIGN KEY ('.$key.') REFERENCES {PRE}'.$value.'");'.PHP_EOL;
		}

		/* set database engine */
		/* $attributes = array("ENGINE" => "InnoDB"); */
		$file_content .= "\t\t".'$attributes = array("ENGINE" => "InnoDB");'.PHP_EOL;

		/* Create table */
		$file_content .= "\t\t".'$this->dbforge->create_table("'.$file_name.'", TRUE, $attributes);'.PHP_EOL;

		$file_content .= "\t".'}'.PHP_EOL.PHP_EOL;

		/* add "Down" function */
		$file_content .= "\t".'public function down()'.PHP_EOL;
		$file_content .= "\t".'{'.PHP_EOL;
		$file_content .= "\t\t".'$this->dbforge->drop_table("'.$file_name.'", TRUE);'.PHP_EOL;
		$file_content .= "\t".'}'.PHP_EOL;

		$file_content .= '}'.PHP_EOL;
		$file_content .= '?>';

		/* Write File to output folder */

		if ( ! write_file($output_folder.date('YmdHis', $version_time + $i).'_add_'.$file_name.'.php', $file_content))
		{
			$fail_cnt++;
			$objPHPExcel->getActiveSheet()->setCellValue('A2', 'FALSE');
		}
		else
		{
			$success_cnt++;
			$objPHPExcel->getActiveSheet()->setCellValue('A2', 'TRUE');
		}

		/* Write File to backup folder*/

		if ( ! write_file($backup_folder.date('YmdHis', $version_time + $i).'_add_'.$file_name.'.php', $file_content))
		{
			$bk_fail_cnt++;
		}
		else
		{
			$bk_success_cnt++;
		}

		$i++;
	}
	else
	{
		$already_generated_cnt++;
		$already_generated_files[] = $file_name;
	}
}

/* update flag is_generated */
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $file_type);
$objWriter->save($input_folder.$input_file);

/* output folder results */

if ( ! empty($fail_cnt))
{
	echo 'Failed to generate: '.$fail_cnt.' file(s). <br/>';
}

if ( ! empty($success_cnt))
{
	echo $success_cnt.' file(s) generated in folder'.$output_folder.'<br/>';
}

/* backup folder results */

if ( ! empty($bk_fail_cnt))
{
	echo 'Failed to generate: '.$bk_fail_cnt.' file(s). <br/>';
}

if ( ! empty($bk_success_cnt))
{
	echo $bk_success_cnt.' file(s) generated in folder'.$backup_folder.'<br/>';
}

/* already generated file data */

if ( ! empty($already_generated_cnt))
{
	echo $already_generated_cnt.' file(s) already generated. <br/>';
}

if ( ! empty($already_generated_files))
{
	echo 'List of file(s) are as follows: <br/>';

	foreach ($already_generated_files as $i => $file)
	{
		echo $i + 1 .'. '.$file.'<br/>';
	}
}

if (empty($success_cnt) && empty($bk_success_cnt))
{
	if (rmdir($backup_folder))
	{
		echo "<br/>No new files generated. <br/> Deleted $backup_folder since folder is empty.";
	}
}
