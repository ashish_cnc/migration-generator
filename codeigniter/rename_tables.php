<?php

/* set time zone */
date_default_timezone_set("Asia/Kolkata");

require_once '../helpers/debug_helper.php';

$project = $_SERVER['QUERY_STRING'];

if( empty($project))
{
	echo 'Project name is empty..Set project name using <b>"?project_name"</b>';
	exit;
}

$project_base_path = '../../' . $project;

if( ! is_dir($project_base_path))
{
	echo 'Project base path does not exist <b>' . $project_base_path . '".</b>Check the project name OR check folder exists or not.';
	exit;
}

$input_folder = $project_base_path . '/migration_generator/';

if( ! is_dir($input_folder))
{
	echo 'Unable to access input folder <b>' . $input_folder . '".</b>Check the project name OR check folder exists or not.';
	exit;
}

$input_file = $project . '_renamed_tables.xlsx';

if( ! file_exists($input_folder . $input_file))
{
	echo 'Input excel file <b>"' . $input_folder . $input_file . '</b> does not exist. Check the file or file name.';
	exit;
}

/* load PHPExcel lib */
require_once '../libraries/PHPExcel.php';

/* generate version time */
$now = getdate();
$version_time = mktime($now['hours'], $now['minutes'], $now['seconds'], $now['mon'], $now['mday'], $now['year']);


/* set output folder */
$output_folder = $project_base_path . '/application/migrations/';

/* generate output folder if not generated */
if( ! is_dir($output_folder))
{
	if(mkdir($output_folder, 0755, true))
	{
		echo "$output_folder created successfully. <br/><br/>";
	}
}

/*  set backup folder */
$backup_folder = $project_base_path . '/migration_backup/' . date('Y-m-d-H-i-s',$version_time) . '_renamed_tables/';

/* generate backup folder if not generated */
if( ! is_dir($backup_folder))
{
	if(mkdir($backup_folder, 0755, true))
	{
		echo "$backup_folder created successfully. <br/><br/>";
	}
}

/* load file helper */
require_once '../helpers/file_helper.php';

/* identify file type and read file */
$file_type	= PHPExcel_IOFactory::identify($input_folder . $input_file);
$objReader	= PHPExcel_IOFactory::createReader($file_type);
$objPHPExcel= $objReader->load($input_folder . $input_file);

// Get total sheet
$i = $fail_cnt = $success_cnt = $bk_fail_cnt = $bk_success_cnt = $already_generated_cnt = 0;
foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
{
	$worksheet_title = $worksheet->getTitle();
	$file_name = ucfirst(strtolower($worksheet_title));
	$sheetIndex = $objPHPExcel->getIndex($worksheet);
	// Set sheet index
	$sheet_data	= $objPHPExcel->setActiveSheetIndex($sheetIndex)->toArray(null,true,true,true);

	$is_generated = $sheet_data[2]['A'];
	$old_table_name = $sheet_data[2]['B'];
	$new_table_name = $sheet_data[2]['C'];
	
	if( ! $is_generated)
	{
		$file_content = '<?php defined(\'BASEPATH\') OR exit(\'No direct script access allowed\');' . PHP_EOL . PHP_EOL;
		$file_content .= 'class Migration_' . $file_name . ' extends CI_Migration' . PHP_EOL;
		$file_content .= '{'. PHP_EOL;
		/* Add up function content */
		$file_content .= "\t" . 'public function up()' . PHP_EOL;
		$file_content .= "\t" . '{' . PHP_EOL;
		
		$file_content .= "\t\t" . '$this->db->query("RENAME TABLE {PRE}' . $old_table_name . ' TO {PRE}' . $new_table_name . '");'. PHP_EOL;
		
		$file_content .= "\t" . '}' . PHP_EOL . PHP_EOL;

		/* add down function content */
		$file_content .= "\t" . 'public function down()' . PHP_EOL;
		$file_content .= "\t" . '{' . PHP_EOL;

		$file_content .= "\t\t" . '$this->db->query("RENAME TABLE {PRE}' . $new_table_name . ' TO {PRE}' . $old_table_name . '");'. PHP_EOL;

		$file_content .= "\t" . '}' . PHP_EOL;

		$file_content .= '}' . PHP_EOL;
		$file_content .= '?>';

		// xdebug($file_content);

		/* Write File to output folder */
		if ( ! write_file($output_folder . date('YmdHis', $version_time + $i) . '_' . strtolower($file_name) . '.php', $file_content))
		{
			$fail_cnt++;
			$objPHPExcel->getActiveSheet()->setCellValue('A2', 'FALSE');
		}
		else
		{
			$success_cnt++;
			$objPHPExcel->getActiveSheet()->setCellValue('A2', 'TRUE');
		}

		/* Write File to backup folder*/
		if ( ! write_file($backup_folder . date('YmdHis', $version_time + $i) . '_' . strtolower($file_name) .'.php', $file_content))
		{
			$bk_fail_cnt ++;
		}
		else
		{
			$bk_success_cnt ++;
		}
		$i++;
	}
	else
	{
		$already_generated_cnt++;
		$already_generated_files[] = $worksheet_title;
	}
}

/* update flag is_generated */
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $file_type);
$objWriter->save($input_folder . $input_file);

/* output folder results */
if( ! empty($fail_cnt))
{
	echo 'Failed to generate: ' . $fail_cnt . ' file(s)';
	echo '<br/>';
}
if( ! empty($success_cnt))
{
	echo $success_cnt . ' file(s) generated in folder' . $output_folder;
	echo '<br/>';
}

/* backup folder results */
if( ! empty($bk_fail_cnt))
{
	echo 'Failed to generate: ' . $bk_fail_cnt . ' file(s)';
	echo '<br/>';
}
if( ! empty($bk_success_cnt))
{
	echo $bk_success_cnt . ' file(s) generated in folder' . $backup_folder;
	echo '<br/>';
}

/* already generated file data */
if( ! empty($already_generated_cnt))
{
	echo $already_generated_cnt . ' file(s) already generated';
	echo '<br/>';
}

if( ! empty($already_generated_files))
{
	echo 'List of file(s) are as follows: ';
	echo '<br/>';
	foreach($already_generated_files as $i => $file)
	{
		echo $i+1 . '. ' . $file;
		echo '<br/>';
	}
}

if(empty($success_cnt) && empty($bk_success_cnt))
{
	if(rmdir($backup_folder))
	{
		echo "<br/>No new files generated. <br/> Deleted $backup_folder since folder is empty.";
	}
}
?>