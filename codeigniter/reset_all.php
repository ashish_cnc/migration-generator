<?php
/* This file is created only for the testing purpose */

/* set time zone */
date_default_timezone_set("Asia/Kolkata");

require_once '../helpers/debug_helper.php';

/* load PHPExcel lib */
require_once '../libraries/PHPExcel.php';

$project = $_SERVER['QUERY_STRING'];

if( empty($project))
{
	exit('Project name is empty..Set project name using <b>"?project_name"</b>');
}

$project_base_path = '../../' . $project;

if( ! is_dir($project_base_path))
{
	exit('Project base path does not exist <b>' . $project_base_path . '".</b>Check the project name OR check folder exists or not.');
}

$input_folder = $project_base_path . '/migration_generator/';

if( ! is_dir($input_folder))
{
	exit('Unable to access input folder <b>' . $input_folder . '".</b>Check the project name OR check folder exists or not.');
}

/********** Creation table excel *************/
$input_file = $project . '.xlsx';

if( ! file_exists($input_folder . $input_file))
{
	echo('Input excel file <b>"' . $input_folder . $input_file . '</b> does not exist. Check the file or file name.' . '<br>');
}
else
{
	/********************** generation_input_file code **********************/

	/* identify file type and read file */
	$file_type		= PHPExcel_IOFactory::identify($input_folder . $input_file);
	$objReader		= PHPExcel_IOFactory::createReader($file_type);
	$objPHPExcel	= $objReader->load($input_folder . $input_file);

	$i = 0;

	// Get total sheet
	foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
	{
		$sheetIndex = $objPHPExcel->getIndex($worksheet);

		// Set sheet index
		$sheet_data	= $objPHPExcel->setActiveSheetIndex($sheetIndex)->toArray(null,true,true,true);
		$is_generated = $sheet_data[2]['A'];
		if($is_generated)
		{
			$objPHPExcel->getActiveSheet()->setCellValue('A2', '');
		}
	}

	/* update flag is_generated */
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $file_type);
	$objWriter->save($input_folder . $input_file);
}

/********** Alternation table excel *************/
$alterations_input_file = $project . '_alterations.xlsx';

if( ! file_exists($input_folder . $alterations_input_file))
{
	echo('Input excel file <b>"' . $input_folder . $alterations_input_file . '</b> does not exist. Check the file or file name.' . '<br>');
}
else
{
	/********************** alterations_input_file code **********************/

	/* identify file type and read file */
	$file_type		= PHPExcel_IOFactory::identify($input_folder . $alterations_input_file);
	$objReader		= PHPExcel_IOFactory::createReader($file_type);
	$objPHPExcel	= $objReader->load($input_folder . $alterations_input_file);

	$i = 0;

	// Get total sheet
	foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
	{
		$sheetIndex = $objPHPExcel->getIndex($worksheet);

		// Set sheet index
		$sheet_data	= $objPHPExcel->setActiveSheetIndex($sheetIndex)->toArray(null,true,true,true);
		$is_generated = $sheet_data[2]['A'];
		if($is_generated)
		{
			$objPHPExcel->getActiveSheet()->setCellValue('A2', '');
		}
	}

	/* update flag is_generated */
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $file_type);
	$objWriter->save($input_folder . $alterations_input_file);
}


/********** Drop table excel *************/
$dropped_tables_input_file = $project . '_dropped_tables.xlsx';

if( ! file_exists($input_folder . $dropped_tables_input_file))
{
	echo('Input excel file <b>"' . $input_folder . $dropped_tables_input_file . '</b> does not exist. Check the file or file name.' . '<br>');
}
else
{
	/******************* dropped_tables_input_file code *******************/

	/* identify file type and read file */
	$file_type		= PHPExcel_IOFactory::identify($input_folder . $dropped_tables_input_file);
	$objReader		= PHPExcel_IOFactory::createReader($file_type);
	$objPHPExcel	= $objReader->load($input_folder . $dropped_tables_input_file);

	$i = 0;

	// Get total sheet
	foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
	{
		$sheetIndex = $objPHPExcel->getIndex($worksheet);

		// Set sheet index
		$sheet_data	= $objPHPExcel->setActiveSheetIndex($sheetIndex)->toArray(null,true,true,true);
		$is_generated = $sheet_data[2]['A'];
		if($is_generated)
		{
			$objPHPExcel->getActiveSheet()->setCellValue('A2', '');
		}
	}

	/* update flag is_generated */
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $file_type);
	$objWriter->save($input_folder . $dropped_tables_input_file);

}


/********** Rename table excel *************/
$renamed_tables_input_file = $project . '_renamed_tables.xlsx';

if( ! file_exists($input_folder . $renamed_tables_input_file))
{
	echo('Input excel file <b>"' . $input_folder . $renamed_tables_input_file . '</b> does not exist. Check the file or file name.' . '<br>');
}
else
{
	/******************* renamed_tables_input_file code *******************/

	/* identify file type and read file */
	$file_type		= PHPExcel_IOFactory::identify($input_folder . $renamed_tables_input_file);
	$objReader		= PHPExcel_IOFactory::createReader($file_type);
	$objPHPExcel	= $objReader->load($input_folder . $renamed_tables_input_file);

	$i = 0;

	// Get total sheet
	foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
	{
		$sheetIndex = $objPHPExcel->getIndex($worksheet);

		// Set sheet index
		$sheet_data	= $objPHPExcel->setActiveSheetIndex($sheetIndex)->toArray(null,true,true,true);
		$is_generated = $sheet_data[2]['A'];
		if($is_generated)
		{
			$objPHPExcel->getActiveSheet()->setCellValue('A2', '');
		}
	}

	/* update flag is_generated */
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $file_type);
	$objWriter->save($input_folder . $renamed_tables_input_file);
}

/* set output folder */
$output_folder = $project_base_path . '/application/migrations/';

/* if folder already exist delete folder for new version */
if(is_dir($output_folder))
{
	array_map('unlink', glob("$output_folder/*.*"));
	if( ! rmdir($output_folder))
	{
		echo ("Could not delete $output_folder. <br/>");
	}
	else
	{
		echo "$output_folder deleted successfully. <br/>";
	}
}

/********************** reset_db code file ************************/
require_once('reset_db.php');
?>