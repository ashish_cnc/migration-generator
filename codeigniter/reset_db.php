<?php
$servername = "localhost";
$username = "root";
$password = "";

$database = $_SERVER['QUERY_STRING'];

if( empty($database))
{
	exit('Project name is empty..Set project name using <b>"?database_name"</b>');
}
// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// Drop database
$sql = "DROP DATABASE $database";
if ($conn->query($sql) === TRUE)
{
    echo "Database dropped successfully <br/>";
}
else
{
    echo "Error dropping database: " . $conn->error . "<br/>";
}

// Create database
$sql = "CREATE DATABASE $database";
if ($conn->query($sql) === TRUE)
{
    echo "Database created successfully <br/>";
}
else
{
    echo "Error creating database: " . $conn->error . "<br/>";
}

$conn->close();
?>