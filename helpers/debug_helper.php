<?php

/* Print the data and continue the execution  */
function debug($array)
{
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}

 /* Print the data and exit */
function xdebug($array)
{
	debug($array);exit;
}

/* Print the data into browser console in json format and continue */
function cdebug($array)
{
	echo "<script>console.log(".json_encode($array, JSON_PRETTY_PRINT).");</script>";
}

/* var_dump data and continue the execution */
function vardump($array)
{
	echo "<pre>";
	var_dump($array);
	echo "</pre>";
}

/* var_dump the data and exit */
function xvardump($array)
{
	vardump($array);exit;
}

/* var_dump the data into browser console in json format and continue */
function cvardump($array)
{
	ob_start();
	var_dump($array);
	$result = ob_get_clean();
	echo "<script>console.log(".json_encode($result, JSON_PRETTY_PRINT).");</script>";
}