For alteration excel sheet follow the following rules:

• Keep alteration type 'add' and 'modify' in different worksheets(Recommended).
• You can add both in same but always put alterations of type 'modify' on top followed by alterations of type 'add'.
• For alteration type 'modify' always put "old_field and new_field, old_field_type, new_field_type and old_field_size and new_field_size" (Even if not modified).
• For foreign keys always specify the constraint number and unsigned if needed.